# YaContestTools

Yet another toolchain for YaContest

Plans:
* Antitask
* Move points from YaContest csv to Google Sheets
* Codestyle tool for C++ task
* Extra. Codestyle tool for multilanguage task
* Extra. Usage of YaContest API for moving to Google Sheets with cron