// Sample generator! Participants will submit  their ones.

#include <iostream>
#include <vector>

std::vector<int> Generate(int n) {
  return {1, 2, 3, 1};
}

int main() {
  int test_num;
  std::cin >> test_num;

  auto test = Generate(test_num);
  std::cout << test.size() << '\n';
  for (int i : test) {
    std::cout << i << '\n';
  }
}
