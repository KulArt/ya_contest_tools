#include <iostream>

int Partition(int* const mas, const int begin, const int end) {
  int pivot = mas[end - 1];
  int piv_ind = begin;
  for (int i = begin; i < end - 1; i++) {
    if (mas[i] < pivot) {
      std::swap(mas[i], mas[piv_ind++]);
    }
  }
  std::swap(mas[piv_ind], mas[end - 1]);
  return piv_ind;
}

void QuickSort(int* const mas, const int begin, const int end) {
  if (end > begin) {
    int i = Partition(mas, begin, end);
    QuickSort(mas, begin, i);
    QuickSort(mas, i + 1, end);
  }
}
int main() {
  int n = 0;
  std::cin >> n;
  auto* mas = new int[n];
  for (int i = 0; i < n; i++) {
    std::cin >> mas[i];
  }
  QuickSort(mas, 0, n);
  for (int i = 0; i < n; i++) {
    std::cout << mas[i] << "\n";
  }
  delete[] mas;
}
