import config

import os
from pathlib import Path


def generate_tests(num_tests, tests_dir="./tests"):
    len_max_num = len(str(num_tests + 1))
    for i in range(1, num_tests + 1):
        extra_len = len_max_num - len(str(i))
        cur_test_filename = '0' * extra_len + str(i)

        if not Path(tests_dir).exists():
            os.system(f"mkdir {tests_dir}")

        if not Path(os.path.join(tests_dir, cur_test_filename)).exists():
            Path(os.path.join(tests_dir, cur_test_filename)).touch()
            Path(os.path.join(tests_dir, cur_test_filename) + '.a').touch()
        with open(os.path.join(tests_dir, cur_test_filename), 'w') as test_file:
            test_file.write(str(i) + '\n')


generate_tests(config.TESTS_NUM)
