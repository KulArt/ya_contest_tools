#!/usr/local/bin/python3.7

import config

import sys

tests_num = config.TESTS_NUM

with open(config.LOG_FILE, 'r+') as f:
    log = f.readlines()
    f.truncate(0)

print(list(map(lambda x: x[:-1], log)))
print("".join(map(lambda x: x[:-1], log)))
if "".join(map(lambda x: x[:-1], log)) != '1' * tests_num:
    sys.exit(0)

sys.exit(1)
