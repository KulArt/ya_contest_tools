import config

import time
import subprocess
import sys


# generates from test_num.txt file with test input.txt
def generate_input(binary_gen_file):
    start_time = time.time()
    try:
        gen_out = subprocess.check_output(f'./{binary_gen_file} > {config.INPUT_TEXT_FILE}; exit 0',
                                          stderr=subprocess.STDOUT, text=True, shell=True, timeout=config.GEN_TL_SEC)
        if gen_out != '':
            print(f"Generation of test failed!")
            print(gen_out)
            sys.exit(1)
    except subprocess.TimeoutExpired:
        pass
    generation_time = round((time.time() - start_time) * 1000)
    print(f"Test generation: {generation_time} ms")
    return generation_time


# produces out_file and write result to it
def generate_result(sol_binary, out_file, is_correct):
    start_time = time.time()
    try:
        sol_out = subprocess.check_output(f'./{sol_binary} > {out_file} < {config.INPUT_TEXT_FILE}; exit 0',
                                          stderr=subprocess.STDOUT, text=True, shell=True, timeout=config.SOL_TL_SEC)
        if sol_out != '':
            sol_type = 'correct' if is_correct else 'bugged'
            print(f"Generation of {sol_type} answer failed! Contact Author, TG: {config.ADMIN_TG}")
            print(sol_out)
            sys.exit(1)
    except subprocess.TimeoutExpired:
        pass
    generation_time = round((time.time() - start_time) * 1000)
    print(f"Answer generation: {generation_time} ms")
    return generation_time


print("Generating test")
gen_time = generate_input(config.GEN_BINARY)
if gen_time > 1000 * config.GEN_TL_SEC:
    print("Test generation timed out!")
    sys.exit(1)

print("Validation test")
val_out = subprocess.check_output(f'./{config.VALIDATOR_BINARY} < {config.INPUT_TEXT_FILE}; exit 0',
                                  stderr=subprocess.STDOUT, text=True, shell=True)
if val_out != '':
    print(f"Validation of test failed!")
    print(val_out)
    sys.exit(1)


print("Correct solution")
gen_time = generate_result(config.CORRECT_SOL_BINARY, config.CORRECT_ANS_FILENAME, True)
if gen_time > 1000 * config.SOL_TL_SEC:
    print(f"Correct solution answer generating timed out! Contact Author, TG: {config.ADMIN_TG}")
    sys.exit(1)

print("Bug solution")
gen_time = generate_result(config.BUGGED_SOL_BINARY, config.BUGGED_ANS_FILENAME, False)
if gen_time > 1000 * config.SOL_TL_SEC:
    print("Bugged solution answer generating timed out!")
    sys.exit(0)
