#include <algorithm>
#include <iostream>
#include <vector>

int main() {
  int n;
  std::cin >> n;
  std::vector<int> res(n, 0);
  for (int i = 0; i < n; ++i) {
    std::cin >> res[i];
  }
  std::sort(res.begin(), res.end());
  for (int i : res) {
    std::cout << i << '\n';
  }
}
