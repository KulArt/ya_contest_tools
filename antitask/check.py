import config


def same_files(participant_lines, jury_lines):
    return participant_lines == jury_lines


def check_files(check):
    with open(config.BUGGED_ANS_FILENAME, 'r') as bugged:
        with open(config.CORRECT_ANS_FILENAME, 'r') as correct:
            return check(bugged.readlines(), correct.readlines())


res = check_files(same_files)
with open('log.txt', 'a') as log:
    log.writelines([str(int(res)) + '\n'])

