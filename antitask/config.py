TESTS_NUM = 30

GEN_FILENAME = 'gen.cpp'
GEN_BINARY = 'gen'

VALIDATOR_FILENAME = 'val.cpp'
VALIDATOR_BINARY = 'val'

CORRECT_SOL_FILENAME = 'correct_sol.cpp'
CORRECT_SOL_BINARY = 'correct'
CORRECT_ANS_FILENAME = 'correct.txt'

BUGGED_SOL_FILENAME = 'bug_sol.cpp'
BUGGED_SOL_BINARY = 'bug'
BUGGED_ANS_FILENAME = 'bug.txt'

GEN_TL_SEC = 1
SOL_TL_SEC = 1
INPUT_TEXT_FILE = 'input.txt'

LOG_FILE = 'log.txt'
ADMIN_TG = '@KulArt'
