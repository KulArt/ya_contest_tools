import config

import subprocess
import sys


def compile_with_log(file, out_file, what, need_to_call_admin):
    print(f'{what} compiling')
    out = subprocess.check_output(f'g++ -std=c++20 -O2 {file} -o {out_file}; exit 0',
                                  stderr=subprocess.STDOUT, text=True, shell=True)
    if out != '':
        print(f"Compilation of {what} failed! {f'Contact Author, TG: {config.ADMIN_TG}' if need_to_call_admin else ''}")
        print(out)
        sys.exit(1)


compile_with_log(config.GEN_FILENAME, config.GEN_BINARY, 'generator', False)
compile_with_log(config.BUGGED_SOL_FILENAME, config.BUGGED_SOL_BINARY, 'correct solution', True)
compile_with_log(config.CORRECT_SOL_FILENAME, config.CORRECT_SOL_BINARY, 'bugged solution', True)
compile_with_log(config.VALIDATOR_FILENAME, config.VALIDATOR_BINARY, 'validator', True)
