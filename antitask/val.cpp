#include "testlib.h"

const int kNumElems = 1e5;
const int kMaxNum = 1e9;

int main(int argc, char* argv[]) {
  registerValidation(argc, argv);
  int n = inf.readInt(1, kNumElems, "n");
  inf.readEoln();

  for (int i = 0; i < n; i++) {
    int a_i = inf.readInt(-kMaxNum, kMaxNum, "a_i");
    inf.readEoln();
  }
  inf.readEof();
}