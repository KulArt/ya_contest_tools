# Antitask

Antitask is task which goal is generating tests for breaking bad solution for given task. Currently available only for C++ tasks.

Runtime files:
* ans_gen.py
* check.py
* config.py
* log.txt

Compile time files:
* build.py
* bug_sol.cpp
* config.py
* correct_sol.cpp
* makefile
* rename.sh
* testlib.h (Thanks to Mike Mirzayanov for amazing library!)
* val.cpp

Postprocessing files:
* postproc.py (must be first in order of adding)
* config.py
* log.txt
